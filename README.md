# Resource Management Configuration Repository

## Description

This repo holds all configuration files for the Spring Config Server

## References

Folder Structure

<https://github.com/spring-cloud/spring-cloud-config/issues/252>
